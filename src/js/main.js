import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import App from './components/App'
import appRedux from './reducers'

import styles from '../css/App.css'

let store = createStore(appRedux);
render (
    <Provider store={store}>
       <App />
    </Provider>, 
    document.getElementById('main')
)

