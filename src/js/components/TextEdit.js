import React, { PropTypes } from 'react';


const TextEdit = ({ text, onChange }) => (
    <div>
        <textarea className='monospace' value={text} onChange={(event) => onChange(event.target.value)} />
    </div>
)

TextEdit.propTypes = {
    text: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
}

export default TextEdit


