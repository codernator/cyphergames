import React, { PropTypes } from 'react';

const SectionTitle = ({ text }) => (
    <h3>{text}</h3>
)

SectionTitle.propTypes = {
    text: PropTypes.string.isRequired
}

export default SectionTitle

