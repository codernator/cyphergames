import React, { PropTypes } from 'react';


const TextView = ({ text }) => (
    <div className='monospace'>{text}</div>
)

TextView.propTypes = {
    text: PropTypes.string.isRequired
}

export default TextView

