import React, { PropTypes } from 'react'

const AppTitle = ({ text }) => (
    <h2>{text}</h2>
)

AppTitle.propTypes = {
    text: PropTypes.string.isRequired
}

export default AppTitle

