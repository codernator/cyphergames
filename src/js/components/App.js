import React from 'react';
import MonoAlphaSimpleApp from '../containers/mono-alpha-simple/App.js';

const App = () => (
    <div>
        <h1>Cypher Games</h1>
        <MonoAlphaSimpleApp />
    </div>
)

export default App

