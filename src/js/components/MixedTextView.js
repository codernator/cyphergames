import React, { PropTypes } from 'react'

const lineSplit = (text, maxTextWidth) =>  {
    // Split text into words by spaces
    let words = text.split(' ')
    let numWords = words.length
    let lastWord = words[words.length - 1]
    let lineWidth = 0
    let wordWidth = 0
    let thisLine = ''
    let allLines = new Array()

    const addToAllLines = (text) => {
        allLines.push(text)
        thisLine = ''
        lineWidth = 0
    }		

    for (let i = 0; i < numWords; i++) {
        let word = words[i]
        thisLine = thisLine.concat(word + ' ')
        lineWidth = thisLine.length
        if (word !== lastWord) {
            let nextWord = words[i + 1]

            if (lineWidth + nextWord.length >= maxTextWidth) {
                addToAllLines(thisLine)
            } else if (word === '~') {
                addToAllLines(' ')
            } else if (nextWord === '~') {
                addToAllLines(thisLine)
            }
        } else {
            addToAllLines(thisLine)
            return allLines
        }
    }
}

class MixedTextView extends React.Component {
    render() {
        let cypherLines = lineSplit(this.props.cypherText, 80)
        let plainLines = lineSplit(this.props.plainText, 80)
        return (
            <div>
            {cypherLines.map(function(line, index) {
                return (
                <div style={{marginBottom: '1em'}}>
                    <div className='monospace' style={{backgroundColor: 'lightgrey'}}>{line}</div>
                    <div className='monospace'>{plainLines[index]}</div>
                </div>
                )
            })}
            </div>
        )
    }
}

MixedTextView.propTypes = {
    plainText: PropTypes.string.isRequired,
    cypherText: PropTypes.string.isRequired,
}

export default MixedTextView


