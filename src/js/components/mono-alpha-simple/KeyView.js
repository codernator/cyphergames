import React, { PropTypes } from 'react'
import * as CypherMachine from '../../cypher/mono-alpha-simple-machine'

const KeyView = ({ cypherKey, iteratorPrefix, onIndexSet }) => (
    <table>
    <tbody>
    <tr>
        <td className='row-header'>Plain</td>
        {CypherMachine.plainCharacters.map(function(letter, index) {
            return (
                <td key={iteratorPrefix+letter}>{letter}</td>
            );
        })}
    </tr>
    <tr>
        <td className='row-header'>Cypher</td>
        {CypherMachine.plainCharacters.map(function(letter, index) {
            let mapped = cypherKey[letter] || "";
            return (
                <td key={iteratorPrefix+letter}>
                    <input type="text" value={mapped} className="key-map-input monospace"
                           onChange={(event) => onIndexSet(letter, event.target.value)} />
                </td>
            );
    })}
    </tr>
    </tbody>
    </table>
)

KeyView.propTypes = {
    // TODO see about specifying Map here
    cypherKey: PropTypes.object.isRequired,
    iteratorPrefix: PropTypes.string.isRequired,
    onIndexSet: PropTypes.func.isRequired
}

export default KeyView

