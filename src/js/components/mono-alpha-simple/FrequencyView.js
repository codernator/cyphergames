import React, { PropTypes } from 'react'
import * as CypherMachine from '../../cypher/mono-alpha-simple-machine'

const FrequencyView = ({ frequencyMap, iteratorPrefix }) => (
    <table>
    <tbody>
    <tr>
        <td className='row-header'>Character</td>
        {CypherMachine.plainCharacters.map(function(letter, index) {
            return (
                <td key={iteratorPrefix+letter} className='padded right-align'>{letter}</td>
            );
        })}
    </tr>
    <tr>
        <td className='row-header'>Frequency</td>
        {CypherMachine.plainCharacters.map(function(letter, index) {
            let mapped = frequencyMap[letter] || "0";
            return (
                <td key={iteratorPrefix+letter} className='monospace padded right-align'>{mapped}</td>
            );
    })}
    </tr>
    </tbody>
    </table>
)

FrequencyView.propTypes = {
    // TODO see about specifying Map here
    frequencyMap: PropTypes.object.isRequired,
    iteratorPrefix: PropTypes.string.isRequired,
}

export default FrequencyView


