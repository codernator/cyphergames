import React, { PropTypes } from 'react'
import TextView from '../TextView'
import MixedTextView from '../MixedTextView'
import TextEdit from '../TextEdit'
import KeyView from './KeyView'
import FrequencyView from './FrequencyView'
import SectionTitle from '../SectionTitle';
import AppTitle from '../AppTitle'
import * as GameMode from '../../constants/cypher-games-modes'

const MonoAlphaSimpleAppView = ({ 
    state, onCypherIndex, onGuessIndex, onPlainTextChange, onSetGameMode 
}) => (
    <app>
    <header>
        <AppTitle text='Mono Alphabetic Cypher Game' />
    </header>
    { state.mode == GameMode.CREATE
        ? 
    <content>
        <SectionTitle text="Plain Text" />
        <TextEdit text={state.plainText} onChange={onPlainTextChange} />
        <SectionTitle text="Actual Key" />
        <KeyView cypherKey={state.actualKey} iteratorPrefix="actual_" onIndexSet={onCypherIndex} />
        <SectionTitle text="Cypher Text" />
        <TextView text={state.cypherText} />
        <button onClick={() => onSetGameMode(GameMode.GUESS)}>Play</button>
    </content>
        : null
    }
    { state.mode == GameMode.GUESS
        ?
    <content>
        <SectionTitle text="Guessed Key" />
        <KeyView cypherKey={state.guessKey} iteratorPrefix="guess_" onIndexSet={onGuessIndex} />
        <SectionTitle text="Cyher and Plain Text" />
        <MixedTextView cypherText={state.cypherText} plainText={state.guessedPlainText} />
        <SectionTitle text="Frequency Map" />
        <FrequencyView frequencyMap={state.frequency} iteratorPrefix="freq_" />
    </content>
        : null
    }
    <footer>
    </footer>
    </app>
)

MonoAlphaSimpleAppView.propTypes = {
    // TODO shape it up
    state: PropTypes.object.isRequired,
    onCypherIndex: PropTypes.func.isRequired,
    onGuessIndex: PropTypes.func.isRequired,
    onPlainTextChange: PropTypes.func.isRequired,
    onSetGameMode: PropTypes.func.isRequired,
}

export default MonoAlphaSimpleAppView;

