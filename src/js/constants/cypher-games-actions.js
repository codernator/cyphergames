
export const SET_MODE_CREATE_CYPHER = 'cypher-games/app/set-mode-create-cypher'
export const SET_MODE_PLAY_GUESS = 'cypher-games/app/set-mode-play-guess'

export const SET_PLAIN_TEXT = "cypher-games/app/set-plain-text"

export const SET_CYPHER_KEY = "cypher-games/app/set-cypher-key"
export const CLEAR_CYPHER_KEY = "cypher-games/app/clear-cypher-key"
export const SET_CYPHER_KEY_INDEX = "cypher-games/app/set-cypher-key-index"
export const CLEAR_CYPHER_KEY_INDEX = "cypher-games/app/clear-cypher-key-index"

export const SET_GUESS_CYPHER_KEY = "cypher-games/app/set-guess-cypher-key"
export const CLEAR_GUESS_CYPHER_KEY = "cypher-games/app/clear-guess-cypher-key"
export const SET_GUESS_CYPHER_KEY_INDEX = "cypher-games/app/set-guess-cypher-key-index"
export const CLEAR_GUESS_CYPHER_KEY_INDEX = "cypher-games/app/clear-guess-cypher-key-index"

