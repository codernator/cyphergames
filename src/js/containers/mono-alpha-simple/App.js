import React from 'react'
import { connect } from 'react-redux'
import * as cypherActions from '../../actions/mono-alpha-simple'
import MonoAlphaSimpleAppView from '../../components/mono-alpha-simple/App.js'

const mapStateToProps = (state) => {
    console.log(state);
    return { state: state.MonoAlphaSimple }
}

const mapDispatchToProps = 
    (dispatch) => {
        return {
            onCypherIndex: (key, value) => {
                console.log(key, value);
                var v = value.trim();
                if (v === "")
                    dispatch(cypherActions.clearCypherKeyIndex(key))
                else
                    dispatch(cypherActions.setCypherKeyIndex(key, v))
            },
            onGuessIndex: (key, value) => {
                console.log(arguments);
                var v = value.trim();
                if (v === "")
                    dispatch(cypherActions.clearGuessCypherKeyIndex(key))
                else
                    dispatch(cypherActions.setGuessCypherKeyIndex(key, v))
            },
            onPlainTextChange: (value) => {
                dispatch(cypherActions.setPlainText(value))
            },
            onSetGameMode: (mode) => {
                dispatch(cypherActions.setGameMode(mode));
            },
        }
    }

const App = connect(mapStateToProps, mapDispatchToProps)(MonoAlphaSimpleAppView)

export default App;

