import * as ActionType from '../constants/cypher-games-actions'
import * as GameMode from '../constants/cypher-games-modes'
import * as CypherMachine from '../cypher/mono-alpha-simple-machine'
import * as Strings from '../util/strings.js'


function countLetters(cypherText) {
    let lookup = { ...CypherMachine.cypherCharacterMap }
    let cc = cypherText.length
    while (cc--) {
        let chr = cypherText.charAt(cc);
        if (chr in lookup)
            lookup[chr]++
    }

    return lookup
}

function newMessage() {
    let plainText = "It's time for Animaniacs - And we're zany to the max - So just sit back and relax - You'll laugh 'til you collapse - We're animan-iacs.  Come join the warner brothers - And the Warner sister, Dot - Just for fun we run around the Warner movie lot - They lock us in the tower - Whenever we get caught - But we break loose and then vamoose - and now you know the plot.  We're animaniacs - Dot is cute and Yakko yaks - Wakko packs away the snacks - While Bill Clinton plays the sax - We're Animaniacs!  Meet Pinky and the Brain who want to rule the universe - Good Feathers flock together, Slappy whacks 'em with her purse - Buttons chases Mindy while Rita sings a verse - The writers flipped we have no script why bother to rehearse!  We're animaniacs - We have pay or play contracts - We're zany to the max - There's baloney in our slacks - We're animany - Totally insaney - Shirley MacClane-y - Animaniacs! Those are the facts."
    let actualKey = Strings.translateKeyFromString("qwertyuiopasdfghjklzxcvbnm");
    let cypherText = CypherMachine.encypher(plainText, actualKey)
    return {
        plainText: plainText,
        cypherText: cypherText,
        frequency: countLetters(cypherText),
        guessedPlainText: CypherMachine.encypher(plainText, {}),
        actualKey: actualKey,
        guessKey: {},
        mode: GameMode.CREATE,
    }
}

function setPlainText(plainText, state) {
    let cypherText = CypherMachine.encypher(plainText, state.actualKey)

    return { 
        ...state,
        plainText: plainText,
        cypherText: cypherText,
        frequency: countLetters(cypherText),
        guessedPlainText: CypherMachine.decypher(cypherText, state.guessKey),
    }
}

function setActualKey(actualKey, state) {
    let cypherText = CypherMachine.encypher(state.plainText, actualKey)

    return { 
        ...state, 
        actualKey: actualKey,
        cypherText: cypherText,
        frequency: countLetters(cypherText),
        guessedPlainText: CypherMachine.decypher(cypherText, state.guessKey),
    }
}

function setGuessKey(guessKey, state) {
    return { 
        ...state,
        guessKey: guessKey,
        guessedPlainText: CypherMachine.decypher(state.cypherText, guessKey),
    }
}


const reducer = (state = newMessage(), action) => {
    switch (action.type) {
        case ActionType.SET_MODE_CREATE_CYPHER:
            return newMessage()

        case ActionType.SET_MODE_PLAY_GUESS:
            return {
                ...state,
                mode: GameMode.GUESS
            }


        case ActionType.SET_PLAIN_TEXT: 
            return setPlainText(action.plainText, state)


        case ActionType.SET_CYPHER_KEY:
            return setActualKey(action.cypherKey, state)

        case ActionType.CLEAR_CYPHER_KEY:
            return setActualKey({}, state)

        case ActionType.SET_CYPHER_KEY_INDEX: 
            return setActualKey(
                    CypherMachine.setKeyIndex({ ...state.actualKey }, action.index, action.value), state) 

        case ActionType.CLEAR_CYPHER_KEY_INDEX: 
            return setActualKey(
                    CypherMachine.clearKeyIndex({ ...state.actualKey }, action.index), state)


        case ActionType.SET_GUESS_CYPHER_KEY:
            return setGuessKey(action.cypherKey, state)

        case ActionType.CLEAR_GUESS_CYPHER_KEY:
            return setGuessKey({}, state)

        case ActionType.SET_GUESS_CYPHER_KEY_INDEX: 
            return setGuessKey(
                   CypherMachine.setKeyIndex({ ...state.guessKey }, action.index, action.value), state)

        case ActionType.CLEAR_GUESS_CYPHER_KEY_INDEX: 
            return setGuessKey(CypherMachine.clearKeyIndex({ ...state.guessKey }, action.index), state)


        default:
            return state
	}
}

export default reducer

