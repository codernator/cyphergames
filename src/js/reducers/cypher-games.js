import * as ActionTypes from '../constants/cypher-games-actions'
import * as GameModes from '../constants/cypher-games-modes'


function createNewState() {
    return {
        mode: GameModes.CREATE
    }
}

function changeGameMode(mode, state) {
    return {
        ...state,
        mode: mode
    }
}


const reducer = (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.SET_MODE_CREATE_CYPHER:
            return changeGameMode(GameModes.CREATE, state)
        case ActionTypes.SET_MODE_PLAY_GUESS:
            return changeGameMode(GameModes.GUESS, state)
        default:
            return state
    }
}

export default reducer

