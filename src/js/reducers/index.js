import { combineReducers } from 'redux'
import MonoAlphaSimple from './mono-alpha-simple'

const app = combineReducers({
    MonoAlphaSimple
})

export default app

