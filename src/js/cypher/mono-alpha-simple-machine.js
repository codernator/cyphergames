export const plainCharacters = "abcdefghijklmnopqrstuvwxyz".split("")
export const plainCharacterMap = plainCharacters.reduce(function(r, v) {
    r[v] = 0
    return r
}, {})

export const cypherCharacters = "abcdefghijklmnopqrstuvwxyz".split("")
export const cypherCharacterMap = cypherCharacters.reduce(function(r, v) { 
    r[v] = 0
    return r
}, {})

export const clearKeyIndex = (key, index) => {
    delete key[index]
    return key
}

export const setKeyIndex = (key, index, value) => {
    if (!(index in plainCharacterMap)) {
        if (index in key)
            delete key[index]
        return key
    }

    let latest = value[value.length -1].toLowerCase()
    if (latest in cypherCharacterMap) {
        key[index] = latest
    }
    return key
}

export const encypher = (plainText, key) => {
    var stream = plainText.toLowerCase().split("").reduce((cypher, character, index) => {
        if (!(character in plainCharacterMap)) {
            cypher.push(character)
        } else {
            cypher.push(key[character] || "_")
        }
        return cypher
    }, [])

    return stream.join("")
}

export const decypher = (cypherText, key) => {
    // The key is a map of plain text to cypher text. To decypher, we need
    // a map of cypher text to plain text.
    let transposed = Object.keys(key).reduce((result, kk) => { 
        result[key[kk]] = kk
        return result 
    }, {})

    return encypher(cypherText, transposed)
}

