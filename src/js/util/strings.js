const ascii_a = "a".charCodeAt(0)
export const translateKeyFromString = (text, reverse = false) => {
    return text.toLowerCase().split("").reduce((result, entry, index) => {
        var index = String.fromCharCode(ascii_a + index)
        if (reverse)
            result[entry] = index
        else
            result[index] = entry

        return result
    }, {})
}

