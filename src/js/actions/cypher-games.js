import * as ActionType from '../constants/cypher-games-actions'
import * as GameMode from '../constants/cypher-games-modes'


export const setGameMode = (gameMode) => {
    switch (gameMode) {
      case GameMode.CREATE:
          return {
              type: ActionType.SET_MODE_CREATE_CYPHER
          }
      case GameMode.GUESS:
          return {
              type: ActionType.SET_MODE_PLAY_GUESS
          }
      default:
          throw new Exception("Game mode " + gameMode + " not ipmlemented.");
    }
}


export const setPlainText = (plainText) => {
    return {
        type: ActionType.SET_PLAIN_TEXT,
        plainText: plainText,
    }
}


export const setCypherKey = (keystring) => {
    throw new Exception("Not implemented.");
}

export const clearCypherKey = () => {
    return {
        type: ActionType.CLEAR_CYPHER_KEY,
    }
}

export const setCypherKeyIndex = (index, value) => {
    return {
        type: ActionType.SET_CYPHER_KEY_INDEX,
        index: index,
        value: value,
    }
}

export const clearCypherKeyIndex = (index) => {
    return {
        type: ActionType.CLEAR_CYPHER_KEY_INDEX,
        index: index,
    }
}


export const setGuessKey = (keystring) => {
    throw new Exception("Not implemented.");
}

export const clearGuessKey = () => {
    return {
        type: ActionType.CLEAR_GUESS_CYPHER_KEY,
    }
}

export const setGuessCypherKeyIndex = (index, value) => {
    return {
        type: ActionType.SET_GUESS_CYPHER_KEY_INDEX,
        index: index,
        value: value,
    }
}

export const clearGuessCypherKeyIndex = (index) => {
    return {
        type: ActionType.CLEAR_GUESS_CYPHER_KEY_INDEX,
        index: index,
    }
}



