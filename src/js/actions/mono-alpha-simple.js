import * as BaseActions from './cypher-games'
import * as ActionType from '../constants/cypher-games-actions'
import * as Strings from '../util/strings.js'

export const setGameMode = BaseActions.setGameMode
export const setPlainText = BaseActions.setPlainText

export const setCypherKey = (keystring) => {
    return {
        type: ActionType.SET_CYPHER_KEY,
        cypherKey: Strings.translateKeyFromString(keystring),
    }
}

export const clearCypherKey = BaseActions.clearCypherKey
export const setCypherKeyIndex = BaseActions.setCypherKeyIndex
export const clearCypherKeyIndex = BaseActions.clearCypherKeyIndex

export const setGuessKey = (keystring) => {
    return {
        type: ActionType.SET_GUESS_CYPHER_KEY,
        cypherKey: Strings.translateKeyFromString(keystring, true),
    }
}

export const clearGuessKey = BaseActions.clearGuessKey
export const setGuessCypherKeyIndex = BaseActions.setGuessCypherKeyIndex
export const clearGuessCypherKeyIndex = BaseActions.clearGuessCypherKeyIndex

